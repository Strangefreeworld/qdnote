from qdnote.newline import newline_string

def test_newline():
    multiple = newline_string(('\r', '\n'))
    assert(multiple == "Multiple")
    unknown = newline_string(None)
    assert(unknown == "Unknown")
    mac = newline_string("\r")
    assert(mac == "Mac (CR)")
    unknown2 = newline_string("hello")
    assert(unknown2 == "Unknown")
    linux = newline_string("\n")
    assert(linux == "Linux (LF)")
