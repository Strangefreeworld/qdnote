import pygments
from qdnote.lexer_utils import default_lexer, lexer_class

def test_lexer():
    item = default_lexer()
    lexer = item()
    assert(lexer.name == "Text only")

def test_lexer_class():
    item = lexer_class("file.rb")
    assert(item == pygments.lexers.RubyLexer)
    item = lexer_class("file.c")
    assert(item == pygments.lexers.CLexer)
    item = lexer_class("file.cr")
    assert(item == pygments.lexers.CrystalLexer)