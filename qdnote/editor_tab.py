import hashlib
import tkinter as tk
import ttkbootstrap as ttk
import os
from chlorophyll import CodeView
from pygments.lexers import get_lexer_for_filename
from lexer_utils import default_lexer #, lexer_class
from check_encoding import detect_encoding
from loguru import logger
from newline import newline_string
from editor_stats import EditorPosition, EditorInformation


class CodeEditorTab(ttk.Frame):
    def __init__(self, parent, file_dir=None):
        ttk.Frame.__init__(self)
        self.parent = parent
        self._file = file_dir
        self._title = "Untitled"
        self._lexer = self._process_file()
        self._editbox = CodeView(self, lexer=self._lexer, undo=True)
        self._editbox.pack(fill="both", expand=True)
        self._newline = newline_string(None)
        if file_dir:
            self.open_file(file_dir)
        text_hash = self._hash_text()
        self._hash = text_hash

    def _process_file(self):
        lexer = default_lexer()
        if self._file:
            # lexer = lexer_class(self._file)
            lexer = get_lexer_for_filename(self._file)
            logger.info(f"File type: {lexer.name}")
        return lexer

    def open_file(self, filepath):
        encoding = detect_encoding(filepath)
        logger.info(f"File encoding: {encoding}")
        with open(filepath, encoding=encoding) as file_handle:
            self._editbox.delete(1.0, tk.END)
            self._editbox.insert(tk.END, file_handle.read())
            self._file = filepath
            self._lexer = self._process_file()
            self._title = os.path.basename(filepath)
            self._hash = self._hash_text()
            self._editbox.configure(lexer=self._lexer)
            self._newline = newline_string(file_handle.newlines)


    def _hash_text(self):
        hasher = hashlib.new("sha1")
        hasher.update(self._editbox.get("1.0", "end").encode("utf-8"))
        return hasher.hexdigest()

    def save_file(self):
        if not self._file:
            raise FileNotFoundError
        with open(self._file, "w") as file_handle:
            file_handle.write(self._editbox.get("1.0", tk.END))
            self._hash = self._hash_text()
            self._title = os.path.basename(self._file)

    def save_file_as(self, filename):
        self._file = filename
        self.save_file()

    def _total_lines(self) -> int:
        return int(self._editbox.index("end-1c").split(".")[0])

    def goto_line(self, line):
        line_count = self._total_lines()
        if line > line_count:
            print("Line number too large")
        else:
            self._editbox.mark_set("insert", f"{line}.0")
            self._editbox.see("insert")

    @property
    def filename(self):
        return self._file

    @property
    def title(self):
        return self._title

    @property
    def modified(self):
        current_hash = self._hash_text()
        return current_hash != self._hash

    def select_all(self):
        self._editbox.tag_add(tk.SEL, "1.0", tk.END)
        self._editbox.mark_set(tk.INSERT, "1.0")
        self._editbox.see(tk.INSERT)
        return "break"

    
    @property
    def text_window(self):
        return self._editbox

    def status_bar_info(self) -> EditorInformation:
        line, col = self._editbox.index(tk.INSERT).split('.')
        position = EditorPosition(line=line, column=col)
        return EditorInformation(
            position=position,
            num_lines=self._editbox.index('end').split('.')[0],
            newline=self._newline,
            modified=self.modified,
            filetype=self._lexer.name
        )

    def undo(self) -> None:
        self._editbox.undo()