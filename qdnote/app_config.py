from pathlib import Path, PurePath
import shutil
from appdirs import AppDirs
from qdnote import __app_name__
from tomlkit import parse, TOMLDocument, dumps

def config_dir():
    appdirs = AppDirs(__app_name__)
    return appdirs.user_config_dir

CONFIG_FILE = PurePath(config_dir(), "qdnote.toml")

def check_config_dir():
    config_path = Path(config_dir())
    if not config_path.exists():
        config_path.mkdir(parents=True)

def config_file_path(file_name: str):
    config_path = Path(config_dir())
    return str(config_path / file_name)


def config_dir_files(file_list):
    check_config_dir()
    current_path = Path(__file__).absolute().parents[0]
    config_path = Path(config_dir())
    for item in file_list:
        config_dir_file = config_path / item
        if not config_dir_file.exists():
            shutil.copyfile(str(current_path / item), config_dir_file)

def read_config() -> TOMLDocument:
    with open(CONFIG_FILE, encoding="utf-8") as config_file:
        return parse(config_file.read())
    
def write_config(data: TOMLDocument) -> None:
    config_text = dumps(data)
    with open(CONFIG_FILE, "w", encoding="utf-8") as config_file:
        config_file.write(config_text)