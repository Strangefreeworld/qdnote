from pathlib import Path
import pygments
import pygments.lexers as lexers
from loguru import logger

LEXER_LIST = list(lexers.get_all_lexers())


def lexer_names():
    return list(map(lambda x: x[0], LEXER_LIST))


class FilenameFilter:
    def __init__(self, filename):
        self._file = filename

    def __call__(self, object):
        value = False
        filename_list = object[2]
        for name in filename_list:
            if not name.startswith("*"):
                if self._file == name:
                    value = True
            if self._file.endswith(name[1:]):
                value = True
        return value


def default_lexer():
    return lexers.TextLexer


def lexer_class(filepath):
    pathobj = Path(filepath)
    filename = pathobj.name
    filter_obj = FilenameFilter(filename)
    results = list(filter(filter_obj, LEXER_LIST))

    logger.info(results)
    if results:
        return lexers.find_lexer_class_by_name(results[0][1][0])
    return default_lexer()


def file_lexer(filepath):
    file_lexer = default_lexer()
    try:
        file_lexer = lexers.find_lexer_class_by_name(filepath)
        logger.info(f"Lexer {file_lexer}")
    except pygments.util.ClassNotFound:
        logger.info("Lexer not found")
    return file_lexer
