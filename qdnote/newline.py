import typing

NEWLINE_TYPE = {
    "\r": "Mac (CR)",
    "\n": "Linux (LF)",
    "\r\n": "Windows (CRLF)"
}

def newline_string(newline):
    if isinstance( newline, typing.Tuple):
        return "Multiple"
    if newline is None:
        return "Unknown"
    if newline not in NEWLINE_TYPE:
        return "Unknown"
    return NEWLINE_TYPE[newline]