import tkinter as tk
from qdnote_frame import QDNoteFrame
from qdnote_app import QDNoteApp


def run_qdnote():
    root = tk.Tk()
    app = QDNoteFrame(root)
    root.mainloop()

def run_qdnote_2():
    app = QDNoteApp()
    app.mainloop()
