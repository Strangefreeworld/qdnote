"""Original code from https://github.com/israel-dryer/Notepad-Tk/blob/master/widgets/statusbar.py"""

import tkinter as tk
import ttkbootstrap as ttk
from editor_stats import EditorInformation


class StatusBar(ttk.Frame):
    """Status bar that shows text widget statistics"""
    def __init__(self, master):
        super().__init__(master, relief=tk.SUNKEN )
        self.master = master
        ttk.Separator(self).pack(fill=tk.X, expand=tk.YES)

        self._position_var = tk.StringVar(value="Line:  1  Column:  0")
        ttk.Label(self,
                 textvariable=self._position_var,
                 relief="sunken",
                 anchor=tk.W).pack(side=tk.LEFT)

        self._line_count = tk.StringVar(value="Lines:  0")
        ttk.Label(self,
                 textvariable=self._line_count,
                 relief="sunken",
                 anchor=tk.W).pack(side=tk.LEFT)

        self._modified_var = tk.StringVar(value="  ")
        ttk.Label(self,
                 textvariable=self._modified_var,
                 relief="sunken",
                 width=30,
                 anchor=tk.W).pack(side=tk.LEFT)
        
        self._line_endings = tk.StringVar(value="Unknown")
        ttk.Label(self,
                  textvariable=self._line_endings,
                  relief="sunken",
                  width=40,
                  anchor=tk.W).pack(side=tk.LEFT)

        self._filetype = tk.StringVar(value="Plain Text")
        ttk.Label(self,
                  textvariable=self._filetype,
                  relief="sunken",
                  width=75,
                  anchor=tk.W).pack(side=tk.LEFT)

        # Event binding
        self.pack(side=tk.BOTTOM, fill=tk.X, padx=2, pady=2)

    def update_status(self, status: EditorInformation):
        """Update Status Bar"""
        self._position_var.set(
            f"Line:  {status.position.line}  Column:  {status.position.column}")

        self._line_count.set(f"Lines:  {status.number_lines}")

        self._modified_var.set("Modified" if status.modified else "  ")
        self._line_endings.set(status.newline)
        self._filetype.set(f"{status.filetype} file")

