import os
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog
import ttkbootstrap as ttk
from status_bar import StatusBar
from editor_tab import CodeEditorTab
from qdnote import parent_dir
from lexer_utils import lexer_names
from find_replace_dialog import FindReplaceDialog
from loguru import logger


class QDNoteFrame(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, args)
        self.parent = parent
        self.parent.title("QDNote")
        self.frame = tk.Frame(self.parent)
        self.frame.pack(fill="both", expand=True)
        appimage = tk.PhotoImage(file=os.path.join(parent_dir(), "qdnote.png"))
        parent.iconphoto(False, appimage)
        self.filetypes = (("Normal text file", "*.txt"), ("all files", "*.*"))
        self.init_dir = os.path.join(os.path.expanduser("~"), "Desktop")

        # Create Notebook ( for tabs ).
        self.nb = ttk.Notebook(self.frame)
        self.nb.bind("<Button-2>", self.close_tab)
        self.nb.pack(fill="both", expand=True)
        self.nb.enable_traversal()
        self._status_bar = StatusBar(self.parent)
        # Override the X button.
        self.parent.protocol("WM_DELETE_WINDOW", self.exit)
        self._filetypes = (("Normal text file", "*.txt"), ("all files", "*.*"))

        # Create Menu Bar
        menubar = tk.Menu(self.parent)

        # Create File Menu
        file_menu = tk.Menu(menubar, tearoff=0)
        file_menu.add_command(label="New", accelerator="Ctrl+N", command=self.new_file)
        file_menu.add_command(
            label="Open", accelerator="Ctrl+O", command=self.open_file
        )
        file_menu.add_command(
            label="Save", accelerator="Ctrl+S", command=self.save_file
        )
        file_menu.add_command(label="Save As...", command=self.save_as)
        file_menu.add_command(label="Close", command=self.close_tab)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self.exit)

        # Create Edit Menu
        edit_menu = tk.Menu(menubar, tearoff=0)
        edit_menu.add_command(label="Undo", command=self.undo)
        edit_menu.add_separator()
        edit_menu.add_command(label="Cut", command=self.cut)
        edit_menu.add_command(label="Copy", command=self.copy)
        edit_menu.add_command(label="Paste", command=self.paste)
        edit_menu.add_command(label="Delete", command=self.delete)
        edit_menu.add_command(label="Select All", command=self.select_all)
        edit_menu.add_separator()
        edit_menu.add_command(label="Go To Line", command=self.goto_line)
        edit_menu.add_command(label="Find", command=self.find_replace)

        # Create Format Menu, with a check button for word wrap.
        format_menu = tk.Menu(menubar, tearoff=0)
        self.word_wrap = tk.BooleanVar()
        format_menu.add_checkbutton(
            label="Word Wrap",
            onvalue=True,
            offvalue=False,
            variable=self.word_wrap,
            command=self.wrap,
        )

        # Attach to Menu Bar
        menubar.add_cascade(label="File", menu=file_menu)
        menubar.add_cascade(label="Edit", menu=edit_menu)
        menubar.add_cascade(label="Format", menu=format_menu)
        self.parent.config(menu=menubar)

        # Create right-click menu.
        self.right_click_menu = tk.Menu(self.parent, tearoff=0)
        self.right_click_menu.add_command(
            label="Undo", command=self.undo, accelerator="Ctrl+Z"
        )
        self.right_click_menu.add_separator()
        self.right_click_menu.add_command(
            label="Cut", accelerator="Ctrl+X", command=self.cut
        )
        self.right_click_menu.add_command(
            label="Copy", accelerator="Ctrl+C", command=self.copy
        )
        self.right_click_menu.add_command(
            label="Paste", accelerator="Ctrl+V", command=self.paste
        )
        self.right_click_menu.add_command(label="Delete", command=self.delete)
        self.right_click_menu.add_separator()
        self.right_click_menu.add_command(
            label="Select All", accelerator="Ctrl+a", command=self.select_all
        )

        # Create tab right-click menu
        self.tab_right_click_menu = tk.Menu(self.parent, tearoff=0)
        self.tab_right_click_menu.add_command(label="New Tab", command=self.new_file)
        self.nb.bind("<Button-3>", self.right_click_tab)
        self.frame.bind_all("<Control-n>", self.new_file_hotkey)
        self.frame.bind_all("<Control-o>", self.open_file_hotkey)
        self.frame.bind_all("<Control-s>", self.save_file_hotkey)
        self.frame.bind_all("<Control-x>", self.cut)
        self.frame.bind_all("<Control-c>", self.copy)
        self.frame.bind_all("<Control-v>", self.paste)
        self.frame.bind_all("<Control-z>", self.undohk)
        self.frame.bind_all("<Control-a>", self.select_all)
        self.frame.bind_all("<Control-f>", self.find_replace)

        # Create Initial Tab
        first_tab = CodeEditorTab(self.nb)
        self.nb.add(first_tab, text="Untitled")
        self._status_bar.pack(anchor=tk.S, expand=True)
        self._status_bar.update_status(self.current_tab.status_bar_info())
        self.frame.after(300, self.update_status)
        self._types = lexer_names()
        # logger.info(self._types)

    def open_file_hotkey(self, _):
        self.open_file()

    def find_replace(self, e=None):
        if getattr(self, "findReplace", None):  # Check if `self.findReplace()` exists
            self.findReplace.deiconify()
        else:
            tab = self.current_tab
            self.findReplace = FindReplaceDialog(self.parent, tab.text_window, True)

    def goto_line(self):
        line_number = simpledialog.askinteger("Goto Line", "Line Number:")
        self.current_tab.goto_line(line_number)

    def open_file(self):
        current_tab = self.current_tab
        open_file = filedialog.askopenfilename(
            initialdir=os.chdir(os.path.expanduser("~")),
            title="Open file",
            filetypes=self._filetypes,
        )
        if open_file:
            logger.info(f"Opening file {open_file} in current tab")
            if current_tab.title == "Untitled":
                current_tab.open_file(open_file)
                self.nb.tab(current_tab, text=current_tab.title)
            else:
                new_tab = CodeEditorTab(self.nb, open_file)
                logger.info(f"Opening file {open_file} in new tab")
                self.nb.add(new_tab, text=new_tab.title)
                self.nb.select(new_tab)

    def save_as(self):
        current_tab = self.current_tab
        save_file = filedialog.asksaveasfilename(
            initialdir=self.init_dir,
            title="Select file",
            filetypes=self._filetypes,
            defaultextension=".txt",
        )
        if not save_file:
            return
        current_tab.save_file_as(save_file)
        self.nb.tab(current_tab, text=current_tab.title)

    def save_file_hotkey(self, _):
        self.save_file()

    def save_file(self):
        if not self.current_tab.filename:
            self.save_as()
        else:
            self.current_tab.save_file()

    def new_file_hotkey(self, _):
        self.new_file()

    def new_file(self):
        new_tab = CodeEditorTab(self.nb)
        self.nb.add(new_tab, text="Untitled")
        self.nb.select(new_tab)

    def update_status(self):
        self._status_bar.update_status(self.current_tab.status_bar_info())
        self.frame.after(300, self.update_status)

    def copy(self, _):
        self.current_tab.text_window.event_generate("<<Copy>>")

    def delete(self):
        try:
            self.current_tab.text_window.delete(tk.SEL_FIRST, tk.SEL_LAST)
        except tk.TclError:
            pass

    def cut(self, _):
        self.current_tab.text_window.event_generate("<<Cut>>")

    def wrap(self):
        pass

    def paste(self, _):
        self.current_tab.text_window.event_generate("<<Paste>>")

    def select_all(self, _):
        self.current_tab.select_all()

    def undohk(self, _):
        print("Main window undo")
        self.undo()

    def undo(self):
        print("Main window undo")
        self.current_tab.undo()

    def right_click(self, event):
        self.right_click_menu.post(event.x_root, event.y_root)

    def right_click_tab(self, event):
        self.tab_right_click_menu.post(event.x_root, event.y_root)

    def close_tab(self, event=None):
        if event is None:
            close_tab = self.current_tab
        else:
            try:
                index = event.widget.index("@%d,%d" % (event.x, event.y))
                close_tab = self.nb.nametowidget(self.nb.tabs()[index])
            except tk.TclError:
                return
        if close_tab.modified:
            response = messagebox.askyesnocancel(
                "QDNote",
                f"File {close_tab.title} has changed. Do you want to save changes?",
            )
            if response is None:
                return
            elif response is True:
                if close_tab.title == "Untitled":
                    save_file = filedialog.asksaveasfilename(
                        initialdir=self.init_dir,
                        title="Select file",
                        filetypes=self._filetypes,
                        defaultextension=".txt",
                    )
                    if save_file:
                        close_tab.save_file_as(save_file)
                    else:
                        close_tab.save_file()
                else:
                    close_tab.save_file()
        self.nb.forget(close_tab)

    def exit(self):
        # Check if any changes have been made.
        if self._check_all_tabs():  # self.save_changes():
            self.parent.destroy()
        else:
            return

    def _check_all_tabs(self) -> bool:
        for tab in self.nb.tabs():
            check_tab = self.nb.nametowidget(tab)
            if check_tab.modified:
                result = messagebox.askyesnocancel(
                    "QDNote",
                    f"File {check_tab.title} has changed. Do you want to save changes?",
                )
                if result is None:
                    return False
                elif result:
                    self.nb.select(check_tab)
                    self.save_file()
        return True

    def save_changes(self):
        if self.current_tab.modified:
            response = messagebox.askyesnocancel(
                "QDNote", "Do you want to save changes?"
            )
            if response is None:
                return False
            elif response is True:
                print("Should save")
            else:
                pass

        return True

    # Get the object of the current tab.
    @property
    def current_tab(self):
        return self.nb.nametowidget(self.nb.select())
