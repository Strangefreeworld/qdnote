import chardet


def detect_encoding(file):
    with open(file, "rb") as check_file:
        encoding_results = chardet.detect(check_file.read())
        return encoding_results["encoding"]
