from dataclasses import dataclass

@dataclass
class EditorPosition:
    line: int
    column: int

    def __init__(self, line: int, column: int):
        self.line = line
        self.column = column

@dataclass
class EditorInformation:
    position: EditorPosition
    number_lines: int
    newline: str
    modified: bool
    filetype: str

    def __init__(self, position: EditorPosition,
                 num_lines: int,
                 newline: str,
                 modified: bool,
                 filetype: str):
        self.position = position
        self.number_lines = num_lines
        self.newline = newline
        self.modified = modified
        self.filetype = filetype
