# QDNote

## Purpose

Lightweight text editor; effectively my own TKinter version of notepad++

## Where I found things

 * [Code Editor Window](https://github.com/rdbende/chlorophyll)
 * [Status bar](https://github.com/israel-dryer/Notepad-Tk)
 * [Find and Replace Dialog](https://github.com/CodeYan01/FindReplaceDialog-tkinter/blob/master/FindReplaceDialog.py)
 * [Setting window icon under Linux/Ubuntu](https://stackoverflow.com/questions/16081201/setting-application-icon-in-my-python-tk-base-application-on-ubuntu)

## Things TODO

 * Test Find/Replace
 * Build out menus; switch highlighting
 * Config stuff

## Notes
 * I tried to use grid, but that is very twitchy, so I used pack